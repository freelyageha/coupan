source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.1.2'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
gem 'jquery-ui-rails'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder'

gem 'rest-client'
gem 'kaminari'
gem 'mysql2'

# web server
gem 'unicorn'
gem 'unicorn-rails'

# HAML simple HTML builder
gem 'haml'

# less: css preprocessor
gem 'less-rails'
gem 'font-awesome-rails'

# css compressor
gem 'yui-compressor'

# unicode encoding
gem 'iconv'

gem 'devise'
gem 'oauth2'
gem 'omniauth'
gem 'omniauth-facebook'
gem 'omniauth-twitter'
gem 'omniauth-kakao'

#gem 'activeadmin'

# dump database
gem 'yaml_db', github: 'jetthoughts/yaml_db', ref: 'fb4b6bd7e12de3cffa93e0a298a1e5253d7e92ba'

group :development, :test do
  gem 'nokogiri'
  gem 'capistrano'
  gem 'minitest'
  gem 'minitest-rails'
  gem 'minitest-reporters'
  gem 'database_cleaner'
  gem "spring"
  gem 'quiet_assets'

  # https://github.com/bmabey/database_cleaner
  gem 'sqlite3'
  gem 'activerecord-deprecated_finders'

  # better debugging
  # http://www.sitepoint.com/silver-bullet-n1-problem/
  gem 'bullet'
  gem 'logger-colors'
  gem 'better_errors'
  gem "binding_of_caller"
  gem 'pry'
  gem 'interactive_editor'
  gem 'awesome_print'
end
