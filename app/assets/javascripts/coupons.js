//= require validate-form
//= require scroll_auto_loader
//= require jquery.autocomplete.min
//= require jquery.knob.min

$(document).on("click", ".media[rel='coupon-link']", function(e) {
  if (e.target.className == 'phone-number') {
    return true;
  }

  location.href = $(this).data('url');
});

$(document).on("change", "#update_stamp input", function(e) {
  $('#update_stamp').submit();
});

$(document).on('submit', '#update_stamp', function(e) {
  var formData = $(this).serialize();
  $.ajax({
    method: "PUT",
    url: $(this).attr('action'),
    data: formData,
    beforeSend: function() {
      $(".btn-status").text("Adjusting...");
      $(".btn-status").attr("disabled", true);
      $("#loading-recommend-spinner").show();
      $('#coupon-recommend-list').children().remove();
    }
  })
    .done(function(html, status) {
      $("#loading-recommend-spinner").hide();
      $('#coupon-recommend-list').append($(html));
    })
    .fail(function() {
      alert('Something wrong update your coupon. Please try again.')
    })
    .always(function() {
      $(".btn-status").text("사용하기");
      $(".btn-status").attr("disabled", false);
    })
    ;

  return false;
});

$(document).on('show.bs.modal', '#appendStampModal', function () {
  $(".dial").knob();
});

$(document).on('hide.bs.modal', '#appendStampModal', function () {
  $(".append-stamp").removeClass("active");
});


$(document).on("click", ".change-maximum-stamp", function() {
  var maximum_dial = parseInt($(this).parents().closest(".modal").find(".dial").val());
  var $stamp_wrapper = $(".btn-group");
  var $stamps = $stamp_wrapper.find(".stamp-btn");
  var $stamp_append_btn = $stamp_wrapper.find(".append-stamp");

  if ($stamps.length > maximum_dial) {
    var n =  $stamps.length - maximum_dial;
    for (i = 0; i < n; i++) {
      $stamps[maximum_dial + i].remove();
    }
  } else if ($stamps.length < maximum_dial) {
    var n = maximum_dial - $stamps.length;
    for (i = 0; i < n; i++) {
      var $l = $("<label class='btn btn-sm btn-default stamp-btn'><input name='coupon[stamp][]' value='" + ($stamps.length + i + 1) + "' type='checkbox' />쿠폰</label>")
      $stamp_append_btn.before($l)
    }
  }
});

$(document).ready(function() {
  $('a[rel="popover"]').popover();
  $('#coupon-list').mixitup();

  $('input.autocomplete').autocomplete({
    serviceUrl: '/api/stores'
  });
});
