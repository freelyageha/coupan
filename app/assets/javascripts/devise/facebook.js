function startConnect(){
  if (/iphone|ipad|ipod/i.test(navigator.userAgent.toLowerCase()) && /CriOS/.test(navigator.userAgent)) {
    window.location = "https://www.facebook.com/dialog/oauth" +
      '?client_id=' + FACEBOOK_APP_ID + 
      '&redirect_uri=' + location.protocol + "//" + location.host + FACEBOOK_LOGIN_URL + 
      '&scope=email&response_type=token&display=popup&return_session=1&session_version=3';
    }

  // Opera mini does not operate facebook javascript sdk login
  else if (/Opera Mini\/|MSIE 7\./.test(navigator.userAgent)) {
    window.location = location.protocol + "//" +
      location.host + FACEBOOK_LOGIN_URL;
  }
  else {
    FB.login(function(response) {
      if (response.authResponse) {
        window.location = FACEBOOK_LOGIN_URL +
          '?access_token='+ response.authResponse.accessToken +
          '&expires='+ response.authResponse.expiresIn +
          '&signed_request='+ response.authResponse.signedRequest;
        }
    }, {scope: "email"});
  }
}

window.fbAsyncInit = function() {
    FB.init({
        appId: FACEBOOK_APP_ID,
        status: true,
        cookie: true,
        xfbml: true,
        version: 'v2.0',
        oauth: true
    });
    FB.Canvas.setAutoGrow();
};

(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ko_KR/sdk.js";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
