function scrollHandler () {
  if (($(document).height() - 500) <= $(window).scrollTop() + $(window).height()) {
    var $pager = $('.pager');
    if ($pager.length) {
      var next_page_uri = $pager.attr('href');
      $.ajax({
        url: next_page_uri,
        beforeSend: function(xhr) {
          $(window).unbind('scroll');
        }
      })
        .done(function( data ) {
          $pager.remove();
          $('#coupon-list').append(data);
          if (data) {
            $(window).bind('scroll', scrollHandler);
          }
        });
    }
    else {
      $(window).unbind('scroll');
    }
  }
}

$(document).ready(function() {
  if ($('.pager')) {
    $(window).bind('scroll', scrollHandler);
  }
})
