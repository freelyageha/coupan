class ApisController < ApplicationController
  @@cache = ActiveSupport::Cache::MemoryStore.new(expires_in: 10.seconds)

  def stores
    query = params[:query]
    stores = @@cache.fetch("stores") do
      Store.all.select(:id, :name, :address_id).map do |s|
        { value: s[:name], data: s.slice(:id, :address_id) }
      end
    end

    render json: {
      suggestions: stores.select { |s| s[:value].include? query }
    }
  end

  def companies
    query = params[:query].upcase
    companies = @@cache.fetch("compnaies") do
      Company.where.not(name: ["", nil]).pluck(:name).map do |s|
        name = s.upcase
        { value: name, data: name }
      end
    end

    render json: {
      suggestions: companies.select { |s| s[:value].include? query }
    }
  end

end
