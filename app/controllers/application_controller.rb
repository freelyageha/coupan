class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :authenticate_user!

  rescue_from Exception do |ex|
    reset_session
    raise ex
  end

  def login_required
    unless user_signed_in?
    end
  end
end
