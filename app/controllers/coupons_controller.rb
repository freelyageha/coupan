class CouponsController < ApplicationController

  skip_before_action :authenticate_user!, only: [:index, :me, :show]
  before_action :get_places, only: [:new, :index]

  def index
    @coupons = Coupon.all
    if params[:place]
      @coupons = @coupons.eager_load(:store).where("stores.address_id=%s", params[:place])
    end

    @coupons = @coupons.page(params[:page])
    if request.xhr?
      return render 'coupons/_list', layout: false
    end
  end

  def destroy
    coupon = Coupon.find params[:id]
    coupon.destroy! if current_user == coupon.user

    redirect_to user_coupons_path(current_user)
  end

  def me
    @user = User.find params[:id]
    @coupons = Coupon.where(user_id: params[:id])
    uniq_address = @coupons.eager_load(:store).uniq.pluck("stores.address_id")
    @places = Address.where(id: uniq_address).select([:id, :name])

    if request.xhr?
      return render 'coupons/me/_list', layout: false
    end
    render 'me'
  end

  def show
    @coupon = Coupon.find params[:id]
    @recommend_coupons = @coupon.recommend
  end

  def update
    @coupon = Coupon.find params[:id]
    if params[:coupon].nil?
      stamp_count = 0
    else
      stamp_count = params[:coupon][:stamp].length
    end

    @coupon.update(stamp: stamp_count) if current_user == @coupon.user
    @recommend_coupons = @coupon.recommend
    render "coupons/_recommend_list", layout: false
  end

  def new
  end

  def create
    coupon = Coupon.new(coupon_params)
    if coupon.save
      return redirect_to user_coupons_path current_user
    end

    render 'coupon/new'
  end


  private def coupon_params
    user = current_user
    coupon = params[:coupon]
    max_stamp = coupon[:max_stamp]
    stamps = coupon[:stamp]
    store_name = coupon[:store] || ""
    store = Store.where(
        name: store_name.strip,
        max_stamp: max_stamp,
        address_id: coupon[:address]
      ).first_or_create
    store.save!

    params.require(:coupon).permit(:user, :stamp, :store).merge(
        user: user,
        stamp: stamps.count,
        store: store
      )
  end

  private def get_places
    @places = Address.all
  end

end
