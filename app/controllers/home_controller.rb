class HomeController < ApplicationController

  skip_before_action :authenticate_user!, only: [:google_valification]

  def index
    return redirect_to user_coupons_path current_user
  end

  def oauth_redirect
    redirect_to '/users/auth/kakao/callback'
  end

  def google_valification
    render 'google_valification', layout: false
  end

end
