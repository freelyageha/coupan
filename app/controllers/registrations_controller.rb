class RegistrationsController < Devise::RegistrationsController
  def update
    user = User.find current_user
    user.update user_params

    flash[:user_update] = "사용자 정보를 정상적으로 저장하였습니다."
    return redirect_to edit_user_registration_path
  end

  private def user_params
    company = Company.find_or_create_by(company_params)
    params.require(:user)
      .permit(:phone_number, :kakao_id)
      .merge(company_id: company.id)
  end

  private def company_params
    cp = params.require(:company).permit(:name)
    cp[:name] = cp[:name].upcase
    cp
  end

end
