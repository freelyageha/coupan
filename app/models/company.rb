class Company < ActiveRecord::Base
  has_many :users
  belongs_to :address

  validates :name, uniqueness: {
    scope: [:address_id], case_sensitive: false
  }

end
