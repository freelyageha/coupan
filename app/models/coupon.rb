# 164,1512,3698,155,3556,
# 3104
class Coupon < ActiveRecord::Base
  belongs_to :user
  belongs_to :store

  default_scope { order('coupons.created_at DESC') }

  def score
    @score
  end
  def score=(score)
    @score = score
  end

  def recommend
    user_id = self.user_id
    store_id = self.store_id
    stamp = self.stamp
    max_stamp = self.store.max_stamp

    my_gender = User.find(self.user_id).gender
    my_coupons = {}
    for coupon in Coupon.where(:user_id => user_id)
      my_coupons[coupon.store_id] = coupon.stamp
    end

    user_ids_filter = Coupon.where(store_id: store_id).where("stamp >= ?", max_stamp - stamp)
    coupons = Coupon
      .where(user_id: user_ids_filter.map{|c| c.user_id})
      .where(store_id: my_coupons.keys)
      .where.not(user_id: user_id)

    other_users = {}
    for user in User.find(user_ids_filter.map{|u| u.user_id})
      other_users[user.id] = user.gender
    end

    scores = {}
    for c in coupons
      sid, uid = c.store_id, c.user_id
      scores[uid] = [] unless scores[uid]
      score = 1.0 - (((my_coupons[sid] + c.stamp) - max_stamp.to_f) / max_stamp.to_f).abs
      score += other_users[uid] != my_gender ? 1.0 : 0.0 unless other_users[uid]
      score += sid == store_id ? 1.0 : 0.0
      scores[uid] << c
      scores[uid][-1].score = score 
    end

	ret = scores.sort_by { |uid, coupons|
    s = coupons.map{|c| c.score}.sum
    s + coupons.size 
  }.reverse
	res = {}
	for (uid, coupons) in ret
	  res[uid] = coupons.sort_by{ |coupon| coupon.score}.reverse
	end
	res
  end

  def self.check_status(user_id)
    # stores = {}
    # for (store_id, user_with_scores) in Coupon.recommend(1, receive_coupon_ids)
    #   stores[store_id] = !user_with_scores.empty?
    # end
    # stores
    coupons = Coupon.where(:user_id => user_id)
    stores = {}
    for my_coupon in coupons
      stores[my_coupon.store_id] =
        Coupon.where(:store_id => my_coupon.store_id)
        .where("stamp >= ?", 10 - my_coupon.stamp).count == 0 ? false : true
    end
    stores
  end
end
