class CreateAddress < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.string :name, uniqueness: true
      t.timestamps
    end
  end
end
