class CreateStore < ActiveRecord::Migration
  def change
    create_table :stores do |t|
      t.string :name, null: false
      t.references :address, null: false
      t.timestamps
    end

    add_index :stores, [:name, :address_id]
  end
end
