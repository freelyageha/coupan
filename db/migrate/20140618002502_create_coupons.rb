class CreateCoupons < ActiveRecord::Migration
  def change
    create_table :coupons do |t|
      t.references :user, null: false
      t.references :store, null: false
      t.integer :stamp

      t.timestamps
    end

    add_index :coupons, [:user_id]
    add_index :coupons, [:store_id]
  end
end
