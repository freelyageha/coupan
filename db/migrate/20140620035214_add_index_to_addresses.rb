class AddIndexToAddresses < ActiveRecord::Migration
  def change
    add_index :addresses, [:name]
  end
end
