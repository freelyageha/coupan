class AddUserColumn < ActiveRecord::Migration
  def change
    add_column :users, :phone_number, :string
    add_column :users, :kakao_id, :string
    add_column :users, :company_name, :string
  end
end
