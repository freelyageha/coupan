class AddLunchTimeToUsers < ActiveRecord::Migration
  def change
    add_column :users, :start_lunch_time, :integer, limit: 2
    add_column :users, :end_lunch_time, :integer, limit: 2
  end
end
