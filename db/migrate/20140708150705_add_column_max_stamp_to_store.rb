class AddColumnMaxStampToStore < ActiveRecord::Migration
  def change
    add_column :stores, :max_stamp, :integer, limit: 2, default: 10
  end
end
