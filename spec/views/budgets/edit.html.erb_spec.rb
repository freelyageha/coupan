require 'spec_helper'

describe "budgets/edit" do
  before(:each) do
    @budget = assign(:budget, stub_model(Budget,
      :year => 1,
      :month => 1,
      :amount => 1
    ))
  end

  it "renders the edit budget form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", budget_path(@budget), "post" do
      assert_select "input#budget_year[name=?]", "budget[year]"
      assert_select "input#budget_month[name=?]", "budget[month]"
      assert_select "input#budget_amount[name=?]", "budget[amount]"
    end
  end
end
