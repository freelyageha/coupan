require 'spec_helper'

describe "budgets/new" do
  before(:each) do
    assign(:budget, stub_model(Budget,
      :year => 1,
      :month => 1,
      :amount => 1
    ).as_new_record)
  end

  it "renders new budget form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", budgets_path, "post" do
      assert_select "input#budget_year[name=?]", "budget[year]"
      assert_select "input#budget_month[name=?]", "budget[month]"
      assert_select "input#budget_amount[name=?]", "budget[amount]"
    end
  end
end
